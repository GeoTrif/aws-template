package com.template.model;

import java.io.Serializable;
import java.util.Objects;

public class Person implements Serializable {

    private int id;
    private String surname;
    private String occupation;
    private String university;

    public Person() {
    }

    public Person(int id, String surname, String occupation, String university) {
        this.id = id;
        this.surname = surname;
        this.occupation = occupation;
        this.university = university;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return id == person.id && Objects.equals(surname, person.surname) && Objects.equals(occupation, person.occupation) && Objects.equals(university, person.university);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, surname, occupation, university);
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", occupation='" + occupation + '\'' +
                ", university='" + university + '\'' +
                '}';
    }
}
