package com.template.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.template.service.SQSService;
import com.template.utils.DataUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SQSController {

    private final SQSService sqsService;
    private final ObjectMapper objectMapper;
    private final DataUtils dataUtils;

    public SQSController(final SQSService sqsService, final ObjectMapper objectMapper, final DataUtils dataUtils) {
        this.sqsService = sqsService;
        this.objectMapper = objectMapper;
        this.dataUtils = dataUtils;
    }

    @GetMapping("/sqs/create-queue")
    public void createQueue(@RequestParam String queueName) {
        sqsService.createQueue(queueName);
    }

    @GetMapping("/sqs/list-all-queues")
    public List<String> listAllQueues() {
        return sqsService.listAllQueues();
    }

    @GetMapping("/sqs/list-queues-using-filter")
    public List<String> listQueuesUsingFilter(@RequestParam String queueNamePrefix) {
        return sqsService.listQueuesUsingFilter(queueNamePrefix);
    }

    @GetMapping("/sqs/get-queue-url")
    public String getQueueUrl(@RequestParam String queueName) {
        return sqsService.getQueueUrl(queueName);
    }

    @GetMapping("/sqs/delete-queue")
    public void deleteQueue(@RequestParam String queueUrl) {
        sqsService.deleteQueue(queueUrl);
    }

    @GetMapping("/sqs/send-message")
    public void sendMessage(@RequestParam String queueUrl, @RequestBody String message) {
        sqsService.sendMessage(queueUrl, message);
    }

    @GetMapping("/sqs/send-multiple-messages")
    public void sendMultipleMessages(@RequestParam String queueUrl, @RequestBody String message) throws JsonProcessingException {
        String personPayload = objectMapper.writeValueAsString(dataUtils.getPersonTestData());

        sqsService.sendMultipleMessages(queueUrl, message, personPayload);
    }

    @GetMapping("/sqs/retrieve-messages")
    public List<String> retrieveMessagesFromQueue(@RequestParam String queueUrl) {
        return sqsService.retrieveMessagesFromQueue(queueUrl);
    }

    @GetMapping("/sqs/delete-messages")
    public void deleteMessagesFromQueue(@RequestParam String queueUrl) {
        sqsService.deleteMessagesAfterRetrieve(queueUrl);
    }
}
