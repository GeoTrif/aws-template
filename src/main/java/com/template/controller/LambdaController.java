package com.template.controller;

import com.template.service.LambdaService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class LambdaController {

    private final LambdaService lambdaService;

    public LambdaController(final LambdaService lambdaService) {
        this.lambdaService = lambdaService;
    }

    @GetMapping("/lambda/list-functions")
    public List<String> listFunctions() {
        return lambdaService.listFunctions();
    }

    @GetMapping("/lambda/delete-function")
    public void deleteFunction(@RequestParam String functionName) {
        lambdaService.deleteFunction(functionName);
    }
}
