package com.template.controller;

import com.template.service.S3Service;
import com.template.utils.FileUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class S3Controller {

    public static final String BUCKET_NAME = "tutorial-bucket-george";

    private final S3Service s3Service;
    private final FileUtils fileUtils;

    public S3Controller(final S3Service s3Service, final FileUtils fileUtils) {
        this.s3Service = s3Service;
        this.fileUtils = fileUtils;
    }

    @GetMapping("/s3/create-bucket")
    public void createBucket() {
        s3Service.createBucket(BUCKET_NAME);
    }

    @GetMapping("/s3/list-all-buckets")
    public void listAllBuckets() {
        s3Service.listAllBuckets();
    }

    @GetMapping("/s3/delete-bucket")
    public void deleteBucket(@RequestParam String bucketName) {
        s3Service.deleteBucket(bucketName);
    }

    @GetMapping("/s3/upload-file")
    public void uploadFile(@RequestParam String fileName) {
        s3Service.uploadObject(BUCKET_NAME, fileName, fileUtils.getTestFile());
    }

    @GetMapping("/s3/delete-object")
    public void deleteObject(@RequestParam String fileName) {
        s3Service.deleteObject(BUCKET_NAME, fileName);
    }

    @GetMapping("/s3/delete-objects")
    public void deleteObjects(@RequestParam String... fileNames) {
        s3Service.deleteObjectsFromBucket(BUCKET_NAME, fileNames);
    }
}
