package com.template.controller;

import com.template.service.KinesisService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class KinesisController {

    private final KinesisService kinesisService;

    public KinesisController(final KinesisService kinesisService) {
        this.kinesisService = kinesisService;
    }

    @GetMapping("/kinesis/send-payload")
    public void sendPayloadsToFirehoseDeliveryStream(@RequestBody String payload) {
        kinesisService.sendMessageToKinesisStream(payload);
    }
}
