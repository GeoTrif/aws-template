package com.template.controller;

import com.template.model.Person;
import com.template.service.AthenaService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AthenaController {

    private final AthenaService athenaService;

    public AthenaController(final AthenaService athenaService) {
        this.athenaService = athenaService;
    }

    @GetMapping("/athena/get-persons")
    public List<Person> getPersons() {
        return athenaService.getPersons();
    }
}
