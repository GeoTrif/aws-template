package com.template;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.athena.AmazonAthena;
import com.amazonaws.services.athena.AmazonAthenaClientBuilder;
import com.amazonaws.services.kinesisfirehose.AmazonKinesisFirehose;
import com.amazonaws.services.kinesisfirehose.AmazonKinesisFirehoseClientBuilder;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class AwsTemplateApplication {

	public static void main(String[] args) {
		SpringApplication.run(AwsTemplateApplication.class, args);
	}

	@Bean
	public AmazonS3 amazonS3Client() {
		return AmazonS3Client.builder()
				.withRegion(Regions.US_EAST_2)
				.build();
	}

	@Bean
	public AmazonAthena amazonAthena() {
		return AmazonAthenaClientBuilder.standard()
				.withRegion(Regions.US_EAST_1)
				.build();
	}

	@Bean
	public AmazonSQS amazonSQS() {
		return AmazonSQSClientBuilder.standard()
				.withRegion(Regions.US_EAST_1)
				.build();
	}

	@Bean
	public AWSLambda awsLambda() {
		return AWSLambdaClientBuilder.standard()
				.withRegion(Regions.US_EAST_1)
				.build();
	}

	@Bean
	public AmazonKinesisFirehose amazonKinesisFirehose() {
		return AmazonKinesisFirehoseClientBuilder.standard()
				.withRegion(Regions.US_EAST_1)
				.build();
	}
}
