package com.template.utils;

import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class FileUtils {

    public File getTestFile() {
        return new File("test-file.csv");
    }
}
