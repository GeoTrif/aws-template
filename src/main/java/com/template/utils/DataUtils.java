package com.template.utils;

import com.template.model.Person;
import org.springframework.stereotype.Component;

@Component
public class DataUtils {

    public Person getPersonTestData() {
        return new Person(1, "John", "Engineer", "Imperial College London");
    }
}
