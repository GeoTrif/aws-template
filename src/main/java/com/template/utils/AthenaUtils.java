package com.template.utils;

import com.amazonaws.services.athena.AmazonAthena;
import com.amazonaws.services.athena.model.*;
import com.template.model.Person;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AthenaUtils {

    private static final int SLEEP_AMOUNT = 5000;
    private static final String FAILED_QUERY_EXECUTION_STATE = "FAILED";
    private static final String CANCELLED_QUERY_EXECUTION_STATE = "CANCELLED";
    private static final String SUCCEEDED_QUERY_EXECUTION_STATE = "SUCCEEDED";
    private static final String CANCELLED_QUERY_EXECUTION_ERROR_MESSAGE = "Query cancelled.";
    private static final String FAILED_QUERY_EXECUTION_ERROR_MESSAGE = "Query failed to run with error message: %s";

    private static final String SUBMIT_ATHENA_QUERY_MESSAGE = "Submitting query: '%s' to %s database: %s";

    private final AmazonAthena amazonAthena;

    public AthenaUtils(final AmazonAthena amazonAthena) {
        this.amazonAthena = amazonAthena;
    }

    public String submitAthenaQuery(String database, String query, String outputFolderPath) {
        System.out.println(String.format(SUBMIT_ATHENA_QUERY_MESSAGE, query, database));

        QueryExecutionContext queryExecutionContext = new QueryExecutionContext();
        queryExecutionContext.withDatabase(database);

        ResultConfiguration resultConfiguration = new ResultConfiguration();
        resultConfiguration.withOutputLocation(outputFolderPath);

        StartQueryExecutionRequest startQueryExecutionRequest = new StartQueryExecutionRequest();
        startQueryExecutionRequest.withQueryString(query);
        startQueryExecutionRequest.withQueryExecutionContext(queryExecutionContext);
        startQueryExecutionRequest.withResultConfiguration(resultConfiguration);

        StartQueryExecutionResult startQueryExecutionResult = amazonAthena.startQueryExecution(startQueryExecutionRequest);

        return startQueryExecutionResult.getQueryExecutionId();
    }

    public void waitForQueryToComplete(String queryExecutionId) throws InterruptedException {
        GetQueryExecutionRequest getQueryExecutionRequest = new GetQueryExecutionRequest();
        getQueryExecutionRequest.withQueryExecutionId(queryExecutionId);

        GetQueryExecutionResult getQueryExecutionResult;

        boolean isQueryStillRunning = true;

        while (isQueryStillRunning) {
            getQueryExecutionResult = amazonAthena.getQueryExecution(getQueryExecutionRequest);
            String queryState = getQueryExecutionResult.getQueryExecution().getStatus().getState();

            switch (queryState) {
                case FAILED_QUERY_EXECUTION_STATE:
                    throw new RuntimeException(String.format(FAILED_QUERY_EXECUTION_ERROR_MESSAGE, getQueryExecutionResult.getQueryExecution().getStatus().getStateChangeReason()));

                case CANCELLED_QUERY_EXECUTION_STATE:
                    throw new RuntimeException(CANCELLED_QUERY_EXECUTION_ERROR_MESSAGE);

                case SUCCEEDED_QUERY_EXECUTION_STATE:
                    isQueryStillRunning = false;
                    break;

                default:
                    Thread.sleep(SLEEP_AMOUNT);
            }
        }
    }

    public List<Person> processResultRows(String queryExecutionId) {
        List<Person> persons = new ArrayList<>();

        GetQueryResultsRequest getQueryResultsRequest = new GetQueryResultsRequest();
        getQueryResultsRequest.withQueryExecutionId(queryExecutionId);

        GetQueryResultsResult getQueryResultsResult = amazonAthena.getQueryResults(getQueryResultsRequest);
        List<Row> rows = getQueryResultsResult.getResultSet().getRows();

        for (int i = 1; i < rows.size(); i++) {
            System.out.println(rows.get(i));
            persons.add(transformRowDataIntoPerson(rows.get(i)));
        }

        return persons;
    }

    private Person transformRowDataIntoPerson(Row row) {
        List<Datum> datum = row.getData();
        Person person = new Person();

        person.setId(Integer.parseInt(datum.get(0).getVarCharValue()));
        person.setSurname(datum.get(1).getVarCharValue());
        person.setOccupation(datum.get(2).getVarCharValue());
        person.setUniversity(datum.get(3).getVarCharValue());

        return person;
    }
}
