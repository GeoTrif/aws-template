package com.template.service;

import com.amazonaws.services.kinesisfirehose.AmazonKinesisFirehose;
import com.amazonaws.services.kinesisfirehose.model.PutRecordBatchRequest;
import com.amazonaws.services.kinesisfirehose.model.Record;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

/**
 * Go into AWS Kinesis service and create a firehose delivery stream.Configure it as you wish, i configured it to put the output in
 * S3 with direct put.
 */
@Service
public class KinesisService {

    private static final String SEND_PAYLOAD_TO_KINESIS_STREAM_MESSAGE = "Sending payload: %s to delivery stream: %s";

    private final AmazonKinesisFirehose amazonKinesisFirehose;
    private final String streamName;

    public KinesisService(final AmazonKinesisFirehose amazonKinesisFirehose,
                          @Value("${aws.kinesis.firehose.streamName}") String streamName) {
        this.amazonKinesisFirehose = amazonKinesisFirehose;
        this.streamName = streamName;
    }

    public void sendMessageToKinesisStream(String payload) {
        Record record = new Record().withData(ByteBuffer.wrap(payload.getBytes(StandardCharsets.UTF_8)));

        PutRecordBatchRequest putRecordBatchRequest = new PutRecordBatchRequest();
        putRecordBatchRequest.withDeliveryStreamName(streamName);
        putRecordBatchRequest.withRecords(record);

        amazonKinesisFirehose.putRecordBatch(putRecordBatchRequest);

        System.out.println(String.format(SEND_PAYLOAD_TO_KINESIS_STREAM_MESSAGE, payload, streamName));
    }

}

/*
Useful docs:
https://docs.aws.amazon.com/sdk-for-java/latest/developer-guide/examples-kinesis.html
 */
