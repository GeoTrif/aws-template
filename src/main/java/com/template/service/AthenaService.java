package com.template.service;

import com.amazonaws.services.athena.model.AmazonAthenaException;
import com.template.model.Person;
import com.template.utils.AthenaUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 1.In order to create a table for testing purpose, create a bucket in AWS S3 and upload the file 'test-file.csv' attached to this project(or put one made by you).
 * 2.From the AWS Athena UI choose 'Create table' and from S3 bucket data.
 * 3.Create a database(use the default if you want), name your table and paste your S3 url(go into s3 where your data is, left click
 * and check S3 URI.)
 * 4.Choose CSV as a data format.
 * 5.In the next step you must specify the columns. I choose id as an int, surname as a string, occupation as a string and university as a string.
 * 6.Leave the table without partitions.
 * 7.After your table was created, check the S3 UI to see if it appears in your list(refresh the list).
 * 8.Click the three dots button in front of your table and press 'Preview table' and check the results.
 */
@Service
public class AthenaService {

    private static final String TEST_DATABASE = "default";
    private static final String TEST_TABLE_QUERY = "SELECT * FROM test_table";
    private static final String TEST_OUTPUT_FOLDER_PATH = "s3://aws-athena-query-results-175008074570-us-east-1";

    private final AthenaUtils athenaUtils;

    public AthenaService(final AthenaUtils athenaUtils) {
        this.athenaUtils = athenaUtils;
    }

    public List<Person> getPersons() {
        String queryExecutionId = athenaUtils.submitAthenaQuery(TEST_DATABASE, TEST_TABLE_QUERY, TEST_OUTPUT_FOLDER_PATH);
        List<Person> persons = new ArrayList<>();

        try {
            athenaUtils.waitForQueryToComplete(queryExecutionId);
            persons = athenaUtils.processResultRows(queryExecutionId);
        } catch (InterruptedException | AmazonAthenaException e) {
            e.printStackTrace();
        }

        return persons;
    }
}
        /*
https://docs.aws.amazon.com/athena/latest/ug/code-samples.html#constants
https://towardsdatascience.com/query-data-from-s3-files-using-aws-athena-686a5b28e943
https://contactsunny.medium.com/how-to-query-athena-from-a-spring-boot-application-93a93c013c85
 */
