package com.template.service;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SQSService {

    private static final String CREATE_QUEUE_MESSAGE = "Creating queue %s.";
    private static final String DELETE_QUEUE_MESSAGE = "Deleting queue %s.";
    private static final String SEND_MESSAGE_TO_QUEUE_MESSAGE = "Sending message: %s to queue url: %s";
    private static final String DELETE_MESSAGES_AFTER_RETRIEVE_MESSAGE = "Deleting messages from queue %s.";
    private static final String SEND_MESSAGES_TO_QUEUE_MESSAGE = "Sending messages: \n %s \n %s \n to queue url: %s";

    private final AmazonSQS amazonSQS;

    public SQSService(final AmazonSQS amazonSQS) {
        this.amazonSQS = amazonSQS;
    }

    public void createQueue(String queueName) {
        System.out.println(String.format(CREATE_QUEUE_MESSAGE, queueName));

        CreateQueueRequest createQueueRequest = new CreateQueueRequest(queueName)
                .addAttributesEntry("DelaySeconds", "5")
                .addAttributesEntry("MessageRetentionPeriod", "86400");

        amazonSQS.createQueue(createQueueRequest);
    }

    public List<String> listAllQueues() {
        ListQueuesResult listQueuesResult = amazonSQS.listQueues();

        return listQueuesResult.getQueueUrls();
    }

    public List<String> listQueuesUsingFilter(String queueNamePrefix) {
        ListQueuesResult listQueuesResult = amazonSQS.listQueues(new ListQueuesRequest(queueNamePrefix));

        return listQueuesResult.getQueueUrls();
    }

    public String getQueueUrl(String queueName) {
        return amazonSQS.getQueueUrl(queueName).getQueueUrl();
    }

    public void deleteQueue(String queueUrl) {
        System.out.println(String.format(DELETE_QUEUE_MESSAGE, queueUrl));

        amazonSQS.deleteQueue(queueUrl);
    }

    public void sendMessage(String queueUrl, String message) {
        System.out.println(String.format(SEND_MESSAGE_TO_QUEUE_MESSAGE, message, queueUrl));

        SendMessageRequest sendMessageRequest = new SendMessageRequest()
                .withQueueUrl(queueUrl)
                .withMessageBody(message)
                .withDelaySeconds(3);

        amazonSQS.sendMessage(sendMessageRequest);
    }

    public void sendMultipleMessages(String queueUrl, String message1, String message2) {
        System.out.println(String.format(SEND_MESSAGES_TO_QUEUE_MESSAGE, message1, message2, queueUrl));

        SendMessageBatchRequest sendMessageBatchRequest = new SendMessageBatchRequest()
                .withQueueUrl(queueUrl)
                .withEntries(new SendMessageBatchRequestEntry("msg1", message1),
                        new SendMessageBatchRequestEntry("msg2", message2).withDelaySeconds(1));

        amazonSQS.sendMessageBatch(sendMessageBatchRequest);
    }

    public List<String> retrieveMessagesFromQueue(String queueUrl) {
        return amazonSQS.receiveMessage(queueUrl).getMessages()
                .stream()
                .map(message -> message.getBody())
                .collect(Collectors.toList());
    }

    public void deleteMessagesAfterRetrieve(String queueUrl) {
        System.out.println(String.format(DELETE_MESSAGES_AFTER_RETRIEVE_MESSAGE, queueUrl));

        amazonSQS.receiveMessage(queueUrl).getMessages()
                .forEach(message -> amazonSQS.deleteMessage(queueUrl, message.getReceiptHandle()));
    }
}

/*
Useful docs:
https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/examples-sqs-message-queues.html
 */
