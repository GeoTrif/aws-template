package com.template.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;

@Service
public class S3Service {

    private static final String DELETING_FILE_SUCCESSFUL_MESSAGE = "%s has been deleted.";
    private static final String CREATING_BUCKET_MESSAGE = "Creating bucket: %s";
    private static final String UPLOADING_FILE_MESSAGE = "Uploading file: %s";
    private static final String UPLOAD_COMPLETE_MESSAGE = "Upload complete.";
    private static final String DELETING_FILE_MESSAGE = "Deleting file: %s";
    private static final String LIST_ALL_BUCKETS_MESSAGE = "%s -> %tF";

    private final AmazonS3 amazonS3;

    public S3Service(final AmazonS3 amazonS3) {
        this.amazonS3 = amazonS3;
    }

    public void createBucket(String bucketName) {
        try {
            CreateBucketRequest createBucketRequest = new CreateBucketRequest(bucketName);
            amazonS3.createBucket(createBucketRequest);

            System.out.println(String.format(CREATING_BUCKET_MESSAGE, bucketName));
        } catch (AmazonS3Exception e) {
            e.printStackTrace();
        }
    }

    public void listAllBuckets() {
        ListBucketsRequest listBucketsRequest = new ListBucketsRequest();
        List<Bucket> buckets = amazonS3.listBuckets(listBucketsRequest);

        buckets.stream().forEach(bucket -> System.out.println(String.format(LIST_ALL_BUCKETS_MESSAGE, bucket.getName(), bucket.getCreationDate())));
    }

    public void deleteBucket(String bucketName) {
        DeleteBucketRequest deleteBucketRequest = new DeleteBucketRequest(bucketName);

        amazonS3.deleteBucket(deleteBucketRequest);
    }

    /**
     * The key is the name of the file that will be uploaded to AWS S3.
     *
     * @param bucketName
     * @param key
     * @param file
     */
    public void uploadObject(String bucketName, String key, File file) {
        System.out.println(String.format(UPLOADING_FILE_MESSAGE, file.getName()));

        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key, file);
        putObjectRequest.withKey(key);

        amazonS3.putObject(putObjectRequest);

        System.out.println(UPLOAD_COMPLETE_MESSAGE);
    }

    public void deleteObject(String bucketName, String key) {
        System.out.println(String.format(DELETING_FILE_MESSAGE, key));

        DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest(bucketName, key);
        deleteObjectRequest.withKey(key);

        amazonS3.deleteObject(deleteObjectRequest);

        System.out.println(String.format(DELETING_FILE_SUCCESSFUL_MESSAGE, key));
    }

    /**
     * The easiest way to delete all objects from a bucket is to delete the bucket.It will delete all the objects from the bucket,
     * then delete the bucket itself.
     *
     * @param bucketName
     * @param keys
     */
    public void deleteObjectsFromBucket(String bucketName, String... keys) {
        DeleteObjectsRequest deleteObjectsRequest = new DeleteObjectsRequest(bucketName);
        deleteObjectsRequest.withKeys(keys);

        amazonS3.deleteObjects(deleteObjectsRequest);
    }
}

/*
Useful docs:
https://docs.aws.amazon.com/sdk-for-java/latest/developer-guide/examples-s3-buckets.html
https://docs.aws.amazon.com/sdk-for-java/latest/developer-guide/examples-s3-objects.html
 */
