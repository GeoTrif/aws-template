package com.template.service;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.model.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 1.Log into AWS.
 * 2.Go to Lambda and create a new function from scratch that has the 'hello world' demo function.
 * 3.After you created your function, go to test and click the test button.
 * 4.If you see an succeeded message,the function was created successfuly.
 */
@Service
public class LambdaService {

    private static final String PAYLOAD = "{\n" +
            " \"Hello \": \"Paris\",\n" +
            " \"countryCode\": \"FR\"\n" +
            "}";

    private final AWSLambda awsLambda;
    private final String functionName;

    public LambdaService(final AWSLambda awsLambda, @Value("${aws.lambda.functionName}") String functionName) {
        this.awsLambda = awsLambda;
        this.functionName = functionName;
    }

    @Scheduled(cron = "${aws.lambda.invokeFunctionCronJob}")
    public void invokeFunction() {
        InvokeRequest invokeRequest = new InvokeRequest();
        invokeRequest.withFunctionName(functionName);
        invokeRequest.withPayload(PAYLOAD);

        InvokeResult invokeResult = null;

        try {
            invokeResult = awsLambda.invoke(invokeRequest);
            String ans = new String(invokeResult.getPayload().array(), StandardCharsets.UTF_8);

            System.out.println(ans);
            System.out.println(invokeResult.getStatusCode());
        } catch (ServiceException e) {
            e.printStackTrace();
        }
    }

    public List<String> listFunctions() {
        ListFunctionsResult listFunctionsResult = awsLambda.listFunctions();
        List<FunctionConfiguration> functionList = listFunctionsResult.getFunctions();

        functionList.forEach(functionConfiguration -> functionConfiguration.getFunctionName());

        return functionList.stream()
                .map(functionConfiguration -> functionConfiguration.getFunctionName())
                .collect(Collectors.toList());
    }

    public void deleteFunction(String functionName) {
        DeleteFunctionRequest deleteFunctionRequest = new DeleteFunctionRequest();
        deleteFunctionRequest.withFunctionName(functionName);

        awsLambda.deleteFunction(deleteFunctionRequest);
        System.out.println("The function was deleted.");
    }
}
